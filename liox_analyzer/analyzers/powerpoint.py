# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from typing import Dict  # pylint: disable=unused-import

from .base import Analyzer
from .mixins import TextAnalyzerMixin
from ..analysis import Analysis  # pylint: disable=unused-import
from ..exceptions import LioxAnalyzerError
from ..extractors import PowerpointExtractor


logger = logging.getLogger(__name__)


class PowerpointAnalyzer(Analyzer, TextAnalyzerMixin):
    id = 'powerpoint-analyzer'
    name = 'Powerpoint Analyzer'
    desc = 'This analyzer handles Powerpoint files.'
    default_extractor = PowerpointExtractor

    def set_units(self, units, sub_units, label):
        # type: (Dict, Dict, str) -> Dict
        sub_words = sub_units[Analysis.UNIT_TYPE_WORDS]
        sub_characters = sub_units[Analysis.UNIT_TYPE_CHARACTERS]

        units[Analysis.UNIT_TYPE_WORDS] += sub_words
        units['{}_words'.format(label)] = sub_words

        units[Analysis.UNIT_TYPE_CHARACTERS] += sub_characters
        units['{}_characters'.format(label)] = sub_characters

        return units

    def analyze(self, file_path):
        # type: (unicode) -> Analysis
        """
        Extractor should return a dictionary with the following keys:

        {
            'slide_words': ,
            'pages': ,
            'note_words': ,
        }

        Returns an Analysis object with extra attributes for power point files.
        These extra attributes are:

        Analysis.slides_words
        Analysis.notes_words
        Analysis.words is a combination of slide_words and notes_words
        """ 
        try:
            extracted_units = self.extractor.extract(file_path)

        except LioxAnalyzerError as e:
            self.errors.append(e)

        if self.errors:
            self.analysis.errors = self.errors
        else:
            units = Analysis.empty_unit_type_dict()

            pages = extracted_units[Analysis.UNIT_TYPE_PAGES]

            slides_units = self.get_units_from_text(extracted_units['slides_words'])
            units = self.set_units(
                units=units,
                sub_units=slides_units,
                label='slides',
            )

            # The library needed to gather note information may not be installed
            notes_words = extracted_units.get('notes_words')
            if notes_words:
                notes_units = self.get_units_from_text(extracted_units['notes_words'])
                units = self.set_units(
                    units=units,
                    sub_units=notes_units,
                    label='notes',
                )

            units[Analysis.UNIT_TYPE_PAGES] = pages
            units[Analysis.UNIT_TYPE_STANDARDIZED_PAGES] = pages
            self.analysis.set_units(units)

        return self.analysis



