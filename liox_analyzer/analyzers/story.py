# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging
import os
import shutil
import zipfile

from time import time
from typing import Type  # pylint: disable=unused-import

from .base import Analyzer
from .story_xml import StoryXMLAnalyzer
from ..constants import IMAGE_FILE_TYPES
from ..analysis import Analysis  # pylint: disable=unused-import
from ..exceptions import FileExtensionNotSupportedError
from ..text_utils import smart_bytes


logger = logging.getLogger(__name__)


TMP_DIR = '/tmp/'


class StoryAnalyzer(Analyzer):
    id = 'story-analyzer'
    name = 'Story Analyzer'
    desc = 'Analyzes a story file'
    out_dir = None  # type: str

    def analyze(self, file_path):
        # type: (unicode) -> Analysis
        """
        Analyzes a story file. Story files are archives, so this analyzer
        loops through all the files in the archive and gathers information about
        them.

        Returns an Analysis object that is an aggregrate of all analyses
        generated by this archive.
        """
        from ..utils import determine_analyzer, extension_from_filename
        timed_at = str(time()).replace('.', '_')
        out_dir = os.path.join(TMP_DIR, timed_at) 

        archive = zipfile.ZipFile(smart_bytes(file_path))

        for file_ in archive.namelist():
            extracted_file = archive.extract(file_, out_dir)

            filename = os.path.basename(extracted_file)
            extension = extension_from_filename(filename)

            # Skip if this is a directory
            if not extension:
                continue

            # Story based XML files need to be handled differently
            if extension == 'xml':
                sub_analysis = StoryXMLAnalyzer().analyze(extracted_file)
            elif extension in IMAGE_FILE_TYPES:
                continue
            else:
           
                try:
                    analyzer = determine_analyzer(extracted_file)
                except FileExtensionNotSupportedError:
                    continue
                else:
                    sub_analysis = analyzer().analyze(extracted_file)

            sub_analysis.filename = filename
            sub_analysis.extension = extension
            self.analysis.analyses.append(sub_analysis)
        
        archive.close()
        shutil.rmtree(out_dir)

        units = self.analysis.sum_analyses()
        self.analysis.set_units(units)

        return self.analysis
