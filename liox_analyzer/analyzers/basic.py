# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from .base import Analyzer
from .mixins import TextAnalyzerMixin
from ..analysis import Analysis  # pylint: disable=unused-import
from ..exceptions import LioxAnalyzerError
from ..extractors.catdoc import CatdocExtractor


logger = logging.getLogger(__name__)


class BasicAnalyzer(Analyzer, TextAnalyzerMixin):
    id = 'basic-analyzer'
    name = 'Basic Analyzer'
    desc = 'This analyzer handles basic word counts'
    default_extractor = CatdocExtractor

    def analyze(self, file_path):
        # type: (unicode) -> Analysis
        """
        Analyzes a file and perform basic text analysis on it.

        Expects a dictionary from the extractor with the following keys:
            text
            pages

        Returns an Analysis object.
        """
        units = Analysis.empty_unit_type_dict()
        
        try:
            extracted_data = self.extractor.extract(file_path)
            text = extracted_data.get('text')
            pages = extracted_data.get(Analysis.UNIT_TYPE_PAGES, 0)
        except LioxAnalyzerError as e:
            self.errors.append(e)

        if self.errors:
            self.analysis.errors = self.errors
        else:
            units = self.get_units_from_text(text)
            units[Analysis.UNIT_TYPE_PAGES] = pages
            units[Analysis.UNIT_TYPE_STANDARDIZED_PAGES] = pages
            self.analysis.set_units(units)

        return self.analysis



