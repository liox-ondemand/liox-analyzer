# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging
import re
import zipfile  # pylint: disable=unused-import

from nltk.tokenize import word_tokenize, sent_tokenize
from typing import Dict, Iterator, List, Tuple, Union  # pylint: disable=unused-import

from ..analysis import Analysis
from ..text_utils import smart_unicode


logger = logging.getLogger(__name__)


PUNCTUATION = (
    '.', '...', '/', '?', '$', ':', ')', '(', ',',
    '[', ']', '\'', '"', '%', '*', "``", "\"\"", "\'\'",
    'gt', 'lt', 'CDATA', '&', '-', u'\u2013', u'\u2014'
)

_digits_pattern = re.compile(r'\d')

EXCLUDE_DO_NOT_TRANSLATE_PATTERN = re.compile(r'\[(do-not-translate|dnt)\](((?!\[\/(do-not-translate|dnt)\]).)|\n)*\[\/(do-not-translate|dnt)\]')


class TextAnalyzerMixin(object):

    def get_units_from_text(self, text):
        # type: (unicode) -> Dict
        """
        Takes supplied text and returns a dictionary containing various unit
        counts.
        """
        units = Analysis.empty_unit_type_dict()

        words, characters, excerpt = self.count_words_chars(text)

        units[Analysis.UNIT_TYPE_WORDS] = words
        units[Analysis.UNIT_TYPE_CHARACTERS] = characters
        units['excerpt'] = excerpt

        return units

    def count_words_chars(self, text):
        # type: (unicode) -> Tuple[int, int, str]
        """
        Accepts a string of ``text`` which is bleached and then tokenized, returning
        a tuple containing the number of words, characters, and an excerpt.
        """
        logger.debug('Counting Words and Characters')

        words = 0
        chars = 0
        excerpt_bits = []  # type: List[str]

        # Strip out [do-not-translate]
        text = smart_unicode(text)
        text = self.exclude_do_not_translate(text)

        for sentence in sent_tokenize(text):
            chars += len([ch for ch in sentence if ch.isalpha()])
            for word in word_tokenize(sentence):
                if len(excerpt_bits) < 50:
                    excerpt_bits.append(word)
                if word not in PUNCTUATION \
                   and not word.isdigit() \
                   and not self.contains_digits(word):
                    words += 1

        excerpt = ' '.join(excerpt_bits)

        return words, chars, excerpt

    def contains_digits(self, text):
        # type: (str) -> bool
        """
        Returns a boolean if text contains digits

        >>> contains_digits("Hello1")
        True
        >>> contains_digits("Hello")
        False
        """
        return bool(_digits_pattern.search(text))

    def exclude_do_not_translate(self, text):
        """
        Returns the text without the 'do-not-translate' and 'dnt' tags

        >>> words = 'Text in [do-not-translate]Do Not Translate[/do-not-translate] and [dnt]the'\
                    'newer "dnt" tags[/dnt] are excluded from the word count.'
        >>> words = exclude_do_not_translate(words)
        >>> print words.split()
        ['Text', 'in', 'and', 'are', 'excluded', 'from', 'the', 'word', 'count.']
        >>> bool(len(words.split()) == 9)
        True
        """
        p = EXCLUDE_DO_NOT_TRANSLATE_PATTERN
        p = p.sub('', text)
        return p
