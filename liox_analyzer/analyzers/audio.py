# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from .base import Analyzer
from ..analysis import Analysis  # pylint: disable=unused-import
from ..exceptions import LioxAnalyzerError
from ..extractors.audio_read import AudioReadExtractor


logger = logging.getLogger(__name__)


class AudioAnalyzer(Analyzer):
    id = 'audio-analyzer'
    name = 'Audio Analyzer'
    desc = 'This analyzer handles audio files'
    default_extractor = AudioReadExtractor

    def analyze(self, file_path):
        # type: (unicode) -> Analysis
        """
        Analyzes an audio file. This implementation only cares about how long
        an audio file is, so we only want to extract the duration from it.

        Returns an Analysis object.
        """
        units = Analysis.empty_unit_type_dict()
        
        try:
            extracted_data = self.extractor.extract(file_path)
            units[Analysis.UNIT_TYPE_MINUTES] = extracted_data.get(Analysis.UNIT_TYPE_MINUTES)
        except LioxAnalyzerError as e:
            self.errors.append(e)

        if self.errors:
            self.analysis.errors = self.errors
        else:
            self.analysis.set_units(units)

        return self.analysis


