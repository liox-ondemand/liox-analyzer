# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging
import zipfile

from typing import AnyStr, Dict  # pylint: disable=unused-import

from .base import Extractor
from ..analysis import Analysis
from ..exceptions import LioxAnalyzerError
from ..text_utils import smart_bytes


logger = logging.getLogger(__name__)


class OdfExtractor(Extractor):
    id = 'odf-extractor'
    name = 'ODF Extractor'
    desc = 'Extracts various pieces of unit data from an odf file.'

    def extract(self, file_path):
        # type: (unicode) -> Dict
        """
        Extracts different unit information from a odf file.

        An OpenDocument file is like a zip archive but with a different extension.
        The files in the zip contain XML files with the text content stored on
        'content.xml' and 'styles.xml' in `<text:p/>` elements.  The `meta.xml` holds the
        file information like character, page and all word count.
        This method returns text and pages.

        Returns a dictionary containing the following keys
        """
        try:
            from bs4 import BeautifulSoup
        except ImportError:
            logger.error(
                'You need to install additional dependencies for this to work. '
                'Try installing the [xml] extras.'
            )
            raise
        try:
            text = ''
            pages = 1

            with zipfile.ZipFile(smart_bytes(file_path)) as odf_file:
                for odf_xml in ('content.xml', 'styles.xml', 'meta.xml'):
                    xml_content = odf_file.read(odf_xml)
                    soup = BeautifulSoup(xml_content, 'xml')
                    
                    if odf_xml == 'meta.xml':
                        pages = int(
                            soup.find('document-statistic').attrs.get(
                                'meta:page-count', 1
                            )
                        )
                    else:
                        text += soup.get_text(separator=' ')
        
            return {
                Analysis.UNIT_TYPE_PAGES: pages,
                'text': text,
            }   
        except Exception:
            logger.exception('An error occurred while extracting %s', file_path)
            raise LioxAnalyzerError(file_path)





