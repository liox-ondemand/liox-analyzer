# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from typing import Dict  # pylint: disable=unused-import

from .base import Extractor
from ..analysis import Analysis
from ..exceptions import LioxAnalyzerError
from ..text_utils import remove_markup_tags


logger = logging.getLogger(__name__)


class XlfExtractor(Extractor):
    id = 'xlf-extractor'  # type: str
    name = 'Xlf File Extractor'  # type: str
    desc = 'Extracts text from an xlf file.'  # type: str

    def extract(self, file_path):
        # type: (unicode) -> Dict
        """
        Extracts the text from an Xlf file.
        """
        try:
            from bs4 import BeautifulSoup
        except ImportError:
            logger.error(
                'You need to install additional dependencies for this to work. '
                'Try installing the [xml] extras.'
            )
            raise
        try:
            text = u''  # type: unicode

            with open(file_path, 'r') as file_obj:
                file_content = file_obj.read()
            soup = BeautifulSoup(file_content, 'xml')

            # Iterate through trans-unit tags
            for trans_unit in soup.find_all('trans-unit'):
                # Check trans-unit tags for translate attribute
                if trans_unit.get('translate', 'yes') == 'no':
                    continue
                # Retrieve text from each <source>
                for contents in trans_unit.find_all('source'):
                # Parse the contents inside `source` to remove html/xml tags
                    text = u'. '.join((text, BeautifulSoup(contents.get_text(), 'lxml').get_text()))

            text = remove_markup_tags(text)
            return {
                'text': text,
                Analysis.UNIT_TYPE_PAGES: 1,
            }
        except Exception:
            logger.exception('An error occurred while extracting %s', file_path)
            raise LioxAnalyzerError(file_path)





