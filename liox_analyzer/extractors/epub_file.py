# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from typing import Dict  # pylint: disable=unused-import

from .base import Extractor
from ..analysis import Analysis
from ..exceptions import LioxAnalyzerError
from ..text_utils import remove_markup_tags


logger = logging.getLogger(__name__)


class EpubExtractor(Extractor):
    id = 'epub-extractor'
    name = 'EPUB File Extractor'
    desc = 'Extracts text from an EPUB file.'

    def extract(self, file_path):
        # type: (unicode) -> Dict
        """
        Extracts the text from an EPUB File.
        """
        try:
            import epub
        except ImportError:
            logger.error(
                'You need to install additional dependencies for this to work. '
                'Try installing the [documents] extras.'
            )
            raise
        try:
            pages = 0
            text = ''

            book = epub.open_epub(file_path, 'r')

            # get titles in metadata
            metadata = book.opf.metadata
            if metadata.titles:
                text += ' '.join(metadata.titles[0])

            # get xhtml and table of contents file
            for item in book.opf.manifest.values():
                if item.media_type == 'application/xhtml+xml' or \
                    item.media_type == 'application/x-dtbncx+xml':
                    try:
                        from bs4 import BeautifulSoup
                    except ImportError:
                        logger.error(
                        'You need to install additional dependencies for this '
                        'to work. Try running `pip install '
                        'liox_file_analyzer[xml]`'
                        )
                        raise

                    item_text = book.read_item(item)
                    soup = BeautifulSoup(item_text)

                    # remove invisible parts
                    for script in soup(["script", "style", "head"]):
                        script.decompose()

                    # read the nav map only for table of contents file
                    if item.media_type == 'application/x-dtbncx+xml':
                        for script in soup(["navlabel"]):
                            text += ' ' + script.extract().get_text()
                    else:
                        text += ' ' + soup.get_text()

                    pages += 1

            text = remove_markup_tags(text)

            return {
                'text': text,
                Analysis.UNIT_TYPE_PAGES: pages,
            }
        except Exception:
            logger.exception('An error occurred while extracting %s', file_path)
            raise LioxAnalyzerError(file_path=file_path)

