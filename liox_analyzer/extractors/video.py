# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

# We need this import here for mypy annotations, but we're ignoring any import
# errors for now because we'll alert the user later on
try:
    from ffvideo import VideoStream  # pylint: disable=unused-import
except ImportError:
    pass

from typing import Dict  # pylint: disable=unused-import

from .base import Extractor
from .audio_read import AudioReadExtractor
from ..analysis import Analysis
from ..exceptions import (
    LioxAnalyzerError,
    VideoDurationError,
    VideoNotValidError,
)
from ..text_utils import smart_bytes

logger = logging.getLogger(__name__)


class VideoExtractor(Extractor):
    id = 'video-extractor'
    name = 'Video Extractor'
    desc = 'Extracts information from a video file using the ffvideo library'

    VIDEO_VALIDATION_WINDOW_SECONDS = 5

    def extract(self, file_path):
        # type: (unicode) -> Dict
        """
        Extracts duration from a video file using the ffvideo library.

        Returns duration in seconds.
        """
        from ..utils import get_minutes

        file_path = smart_bytes(file_path)
        try:
            from ffvideo import VideoStream, DecoderError  # pylint: disable=redefined-outer-name
        except ImportError:
            logger.error(
                'You need to install additional dependencies for this to work. '
                'Try installing the [multimedia] extras.'
            )
            raise

        try:
            video_stream = VideoStream(file_path)
            self._validate_video_file(video_stream)
            return {
                Analysis.UNIT_TYPE_MINUTES: get_minutes(video_stream.duration)
            }
        except (VideoNotValidError, VideoDurationError):
            logger.exception('An error occurred while validating %s', file_path)
            raise
        except DecoderError:
            # Use audio extractor if ffvideo fails to retrieve a video stream.
            audio_extractor = AudioReadExtractor()
            extracted_data = audio_extractor.extract(file_path)
            return {
                Analysis.UNIT_TYPE_MINUTES: extracted_data.get(Analysis.UNIT_TYPE_MINUTES)
            }

        except Exception:
            logger.exception('An error occurred while extracting %s', file_path)
            raise LioxAnalyzerError(file_path)



    def _validate_video_file(self, video_stream):
        # type: (VideoStream) -> None
        """
        Determines if the video is broken or incomplete.
        """
        try:
            frame = None
            framecount = 0
            for frame in video_stream:
                framecount += 1
            last_timestamp = frame.timestamp
        except Exception:
            raise VideoNotValidError(video_stream.filename, msg='Cannot read through frames.')
        
        if video_stream.duration - self.VIDEO_VALIDATION_WINDOW_SECONDS <= last_timestamp <= video_stream.duration + self.VIDEO_VALIDATION_WINDOW_SECONDS:
            pass
        else:
            raise VideoDurationError(
                video_stream.filename,
                msg='Video duration is not close to its last frame timestamp.',
            )

        return




        

