# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import abc
import logging
import six

from typing import Dict  # pylint: disable=unused-import


logger = logging.getLogger(__name__)


@six.python_2_unicode_compatible
class Extractor(object):
    __metaclass__ = abc.ABCMeta
    id = 'file-extractor'  # type: str
    name = 'File Extractor'  # type: str
    desc = None  # type: str

    def __str__(self):
        # type: () -> str
        return self.name

    def __repr__(self):
        # type: () -> str
        return '<{}: {}>'.format(self.name, self.id)

    @abc.abstractmethod
    def extract(self, file_path):
        # type: (unicode) -> Dict
        """
        All work done to extract information from a file should be done in this
        method.
        """
        pass
