# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from typing import Dict  # pylint: disable=unused-import

from .base import Extractor
from ..analysis import Analysis
from ..exceptions import LioxAnalyzerError
from ..text_utils import remove_markup_tags


logger = logging.getLogger(__name__)


class ResExtractor(Extractor):
    id = 'res-extractor'
    name = 'Resource File Extractor'
    desc = 'Extracts text from a resource file using BeautifulSoup.'

    def extract(self, file_path):
        # type: (unicode) -> Dict
        """
        Extracts the text from a Resource file.
        """
        try:
            from bs4 import BeautifulSoup
        except ImportError:
            logger.error(
                'You need to install additional dependencies for this to work. '
                'Try installing the [documents] extras.'
            )
            raise
        try:
            text = ''
            with open(file_path, 'r') as file_obj:
                file_content = file_obj.read()
            soup = BeautifulSoup(file_content)

            for item in soup.find_all('data'):
                if item.has_attr('xml:space') and 'preserve' in item.attrs['xml:space']:
                    text += '. ' + ' '.join([' '.join(value.stripped_strings) for value in item.find_all('value')])

            text = remove_markup_tags(text)
            return {
                Analysis.UNIT_TYPE_PAGES: 1,
                'text': text,
            }
        except Exception:
            logger.exception('An error occurred while extracting %s', file_path)
            raise LioxAnalyzerError(file_path)



