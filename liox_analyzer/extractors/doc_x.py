# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from typing import Dict  # pylint: disable=unused-import
from zipfile import BadZipfile

from .base import Extractor
from ..exceptions import LioxAnalyzerError

from docx.opc.constants import RELATIONSHIP_TYPE as RT
from docx.oxml.table import CT_Tbl
from docx.oxml.text.paragraph import CT_P
from docx.table import _Cell, Table
from docx.text.paragraph import Paragraph

logger = logging.getLogger(__name__)


class DocxExtractor(Extractor):
    id = 'docx-extractor'
    name = 'Docx Extractor'
    desc = 'Extracts text from a file using the docx module'

    def extract(self, file_path):
        # type: (unicode) -> Dict
        """
        Extracts text from a word document using the python-docx module
        """
        try:
            from docx import Document
            from docx.opc.oxml import parse_xml
        except ImportError:
            logger.error(
                'You need to install additional dependencies for this to work. '
                'Try installing the [documents] extras.'
            )
            raise
        try:
            with open(file_path, 'r') as file_:
                document = Document(file_)
            # Get the normal texts from the paragraphs attribute.
            text_list = [p.text for p in document.paragraphs]

            # Get textbox (txbx) texts.
            document_el = parse_xml(document.element.xml)
            # Only WordProcessingShape or `wps` namespace contain TextBox elements.
            if 'wps' in document_el.nsmap:
                # Define TextBox elements in xpath selectors.
                selector = '//wps:txbx//w:txbxContent//w:r//w:t'

                # Use xpath to get the texts
                result = document_el.xpath(selector, namespaces=document_el.nsmap)
                text_list += [el.text for el in result]

            # Get the Header (hdr) texts.
            for header_part in iter_parts(document, RT.HEADER):
                document_el = parse_xml(header_part.blob)
                result = document_el.xpath('//w:hdr//w:t', namespaces=document_el.nsmap)
                text_list += [el.text for el in result if el.text]

            # Get the Footer (ftr) texts.
            for footer_part in iter_parts(document, RT.FOOTER):
                document_el = parse_xml(footer_part.blob)
                result = document_el.xpath('//w:ftr//w:t', namespaces=document_el.nsmap)
                text_list += [el.text for el in result]

            # Get table texts.
            for table in document.element.body.tbl_lst:
                for element in iter_table_items(table, document):
                    text_list += [element.text]

            return {
                'text': ' '.join(text_list),
            }

        except (BadZipfile, KeyError):
            logger.exception('An error occurred while extracting %s', file_path)
            # docx raises a BadZipfile when the format is actually .doc, but
            # the file extension is .docx
            raise LioxAnalyzerError(
                file_path,
                msg='Error Processing docx file, format is not valid',
            )


def iter_parts(document, reltype):
    """ Returns parts of a document. """
    for rel in document.part.rels.values():
        if rel.reltype == reltype:
            yield rel.target_part


def iter_table_items(element, parent_el):
    """
    Generate a reference to each paragraph from nested tables within
    a given ``element``.  Each yielded value is an instance of
    either Table or Paragraph. ``element`` would commonly be a
    reference to a main Table object.
    """
    def _iter_rows(elx, px):
        """ Private method for iterating the table cells. """
        table = Table(elx, px)
        for row in table.rows:
            for cell in row.cells:
                for el in iter_table_items(cell, px):
                    yield el

    if isinstance(element, CT_Tbl):
        for el in _iter_rows(element, parent_el):
            yield el
    elif isinstance(element, _Cell):
        parent_el = element._tc
        for child in parent_el.iterchildren():
            if isinstance(child, CT_P):
                yield Paragraph(child, element)
            elif isinstance(child, CT_Tbl):
                for el in _iter_rows(child, element):
                    yield el
