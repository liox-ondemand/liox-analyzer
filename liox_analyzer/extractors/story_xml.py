# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from typing import Dict  # pylint: disable=unused-import
from xml.dom.minidom import parse

from .base import Extractor
from ..exceptions import LioxAnalyzerError


logger = logging.getLogger(__name__)


class StoryXMLExtractor(Extractor):
    id = 'story-xml-extractor'
    name = 'Story XML Extractor'
    desc = 'Extracts text from an XML file using the xml library.'

    def extract(self, file_path):
        # type: (unicode) -> Dict
        """
        Extracts the text from a Story-based XML file.
        """
        try:
            return {
                'text': parse(file_path)
            }
        except Exception:
            logger.exception('An error occurred while extracting %s', file_path)
            raise LioxAnalyzerError(file_path)


