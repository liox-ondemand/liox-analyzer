# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from typing import Dict  # pylint: disable=unused-import

from .base import Extractor
from ..exceptions import LioxAnalyzerError
from ..text_utils import remove_markup_tags


logger = logging.getLogger(__name__)


class IdmlExtractor(Extractor):
    id = 'idml-extractor'
    name = 'IDML File Extractor'
    desc = 'Extracts text from an IDML file.'

    def extract(self, file_path):
        # type: (unicode) -> Dict
        """
        Extracts the text from an IDML File.
        """
        try:
            import ucf
            from bs4 import BeautifulSoup
        except ImportError:
            logger.error(
                'You need to install additional dependencies for this to work. '
                'Try installing the [xml] extras.'
            )
            raise
        try:
            text = ''
            my_doc = ucf.UCF(filename=file_path)
            for key in my_doc.keys():
                if key.startswith('Stories/'):
                    soup = BeautifulSoup(my_doc[key])
                    for story in soup.find_all('story'):
                        text += '. ' + ' '.join([' '.join(content.stripped_strings) for content in story.find_all('content')])

            text = remove_markup_tags(text)

            return {
                'text': text,
            }
        except Exception:
            logger.exception('An error occurred while extracting %s', file_path)
            raise LioxAnalyzerError(file_path)

