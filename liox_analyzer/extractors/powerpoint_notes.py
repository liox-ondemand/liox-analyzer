# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from typing import Dict  # pylint: disable=unused-import

from .base import Extractor
from ..exceptions import LioxAnalyzerError


logger = logging.getLogger(__name__)


class PowerpointNotesExtractor(Extractor):
    id = 'powerpoint-notes-extractor'
    name = 'Powerpoint Notes Extractor'
    desc = 'Extracts text in notes from a Powerpoint file using the pptx library.'

    def extract(self, file_path):
        # type: (unicode) -> Dict
        """
        Extracts text contained in notes in powerpoint files.
        """
        try:
            from pptx import Presentation
        except ImportError:
            logger.error(
                'You need to install additional dependencies for this to work. '
                'Try installing the [documents] extras.'
            )
            raise

        try:
            presentation = Presentation(file_path)
            text = []
            for slide in presentation.slides:
                try:
                    if slide.has_notes_slide:
                        for paragraph in slide.notes_slide.notes_text_frame.paragraphs:
                            text.append(paragraph.text)
                except AttributeError:
                    logger.exception(
                        'Extracting speaker notes not supported. \
                        Requires python-pptx version 0.6.2 or higher.'
                    )
            return {
                'text': u' '.join(text),
            }
        except Exception:
            logger.exception('An error occurred while extracting %s', file_path)
            raise LioxAnalyzerError(file_path)



