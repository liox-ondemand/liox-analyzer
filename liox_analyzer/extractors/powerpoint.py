# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from typing import Dict  # pylint: disable=unused-import

from .base import Extractor
from .powerpoint_notes import PowerpointNotesExtractor
from .powerpoint_pages import PowerpointPagesExtractor
from .powerpoint_words import PowerpointWordsExtractor
from ..analysis import Analysis
from ..exceptions import LioxAnalyzerError


logger = logging.getLogger(__name__)


class PowerpointExtractor(Extractor):
    id = 'powerpoint-extractor'
    name = 'Powerpoint Extractor'
    desc = 'Extracts various pieces of unit data from a powerpoint file.'

    def extract(self, file_path):
        # type: (unicode) -> Dict
        """
        Extracts different unit information from a powerpoint file. This
        extractor uses other extractors to gather data.

        Returns a dictionary with the following keys:
            - slides_words
            - pages
            - notes_words
        """
        try:
            slides_words = PowerpointWordsExtractor().extract(file_path).get('text')
            pages = PowerpointPagesExtractor().extract(file_path).get('pages')
            notes_words = PowerpointNotesExtractor().extract(file_path).get('text')
            return {
                'slides_words': slides_words,
                Analysis.UNIT_TYPE_PAGES: pages,
                'notes_words': notes_words,
            }   
        except Exception:
            logger.exception('An error occurred while extracting %s', file_path)
            raise LioxAnalyzerError(file_path)




