# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging
import re

from typing import Dict  # pylint: disable=unused-import

from .base import Extractor
from ..analysis import Analysis
from ..exceptions import LioxAnalyzerError
from ..text_utils import remove_markup_tags


logger = logging.getLogger(__name__)


class ErbExtractor(Extractor):
    id = 'erb-extractor'
    name = 'ERB File Extractor'
    desc = 'Extracts text from an erb file.'

    def extract(self, file_path):
        # type: (unicode) -> Dict
        """
        Extracts the text from an ERB File.
        """
        try:
            from bs4 import BeautifulSoup
        except ImportError:
            logger.error(
                'You need to install additional dependencies for this to work. '
                'Try installing the [xml] extras.'
            )
            raise
        try:
            with open(file_path, 'r') as file_obj:
                file_content = file_obj.read()

            text = BeautifulSoup(file_content).get_text()

            # Pulling out ERB Variables and code
            text = re.sub('{.*}', ' ', text)
            text = re.sub('<%=.*%>', ' ', text)

            text = remove_markup_tags(text)

            return {
                'text': text,
                Analysis.UNIT_TYPE_PAGES: 1,
            }
        except Exception:
            logger.exception('An error occurred while extracting %s', file_path)
            raise LioxAnalyzerError(file_path)
