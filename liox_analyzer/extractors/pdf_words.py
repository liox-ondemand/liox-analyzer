# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

from contextlib import closing
import logging

from cStringIO import StringIO
from typing import Dict  # pylint: disable=unused-import

from .base import Extractor
from ..exceptions import LioxAnalyzerError


logger = logging.getLogger(__name__)


class PDFWordExtractor(Extractor):
    id = 'pdf-word-extractor'
    name = 'PDF word Extractor'
    desc = 'Extracts the text from a PDF file using PdfFileReader.'

    def extract(self, file_path):
        # type: (unicode) -> Dict
        """
        Extracts text from a PDF File using the pdfminer library.
        """
        try:
            from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
            from pdfminer.converter import TextConverter
            from pdfminer.layout import LAParams
            from pdfminer.pdfpage import PDFPage
        except ImportError:
            logger.error(
                'You need to install additional dependencies for this to work. '
                'Try installing the [documents] extras.'
            )
            raise

        manager = PDFResourceManager()

        try:
            with closing(StringIO()) as output:
                with closing(TextConverter(manager, output, laparams=LAParams())) as converter:
                    interpreter = PDFPageInterpreter(manager, converter)
                    with open(file_path, 'rb') as file_:
                        for page in PDFPage.get_pages(file_, set()):
                            interpreter.process_page(page)
                        text = output.getvalue()

            return {
                'text': text,
            }
        except Exception:
            logger.exception('An error occurred while extracting %s', file_path)
            raise LioxAnalyzerError(file_path)


        
