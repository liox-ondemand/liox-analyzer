# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import io
import logging

# We need this import here for mypy annotations, but we're ignoring any import
# errors for now because we'll alert the user later on
try:
    from pptx import Presentation  # pylint: disable=unused-import
except ImportError:
    pass

from typing import Dict, Optional, Sequence  # pylint: disable=unused-import

from ..exceptions import LioxAnalyzerError


logger = logging.getLogger(__name__)


class PowerpointMixin(object):

    def create_presentation(self, file_path):
        # type: (unicode) -> Presentation
        try:
            from pptx import Presentation  # pylint: disable=redefined-outer-name
        except ImportError:
            logger.error(
                'You need to install additional dependencies for this to work. '
                'Try installing the [documents] extras.'
            )
            raise
        else:
            return Presentation(file_path)


class MultiCodecExtractorMixin(object):
    """
    Mixin that will attempt to use various codecs to read contents from a file.
    It will use the first codec that is successful.
    """

    def multi_codec_read_file(self, file_path, encoding=None):
        # type: (unicode, Optional[str]) -> str
        """
        Extracts text from a file using io.open. Attempts to use various codecs
        until we find one that matches, unless a specific codec is passed.
        """
        supported_codecs = ('ascii', 'utf-8', 'utf-16', 'iso-8859-1')  # type: Sequence[str] 

        if encoding is not None:
            supported_codecs = (encoding,)

        # Try various supported codecs until we find one that works.
        for codec in supported_codecs:
            try:
                with io.open(file_path, 'r', encoding=codec) as file_:
                    file_content = file_.read()
                return file_content
            except UnicodeDecodeError:
                logger.debug('Decoding error: %s - %s', codec, file_path)
                continue
            except UnicodeError:
                logger.debug('Unicode error: %s - %s', codec, file_path)
                continue
        raise LioxAnalyzerError(file_path=file_path)
