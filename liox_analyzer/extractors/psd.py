# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from typing import Dict, Union  # pylint: disable=unused-import

# We need this import here for mypy annotations, but we're ignoring any import
# errors for now because we'll alert the user later on
try:
    from psd_tools.user_api.psd_image import Group, Layer  # pylint: disable=unused-import
except ImportError:
    pass

from .base import Extractor
from ..analysis import Analysis
from ..exceptions import LioxAnalyzerError


logger = logging.getLogger(__name__)


class PsdExtractor(Extractor):
    id = 'psd-extractor'
    name = 'PSD File Extractor'
    desc = 'Extracts text from a psd file.'

    def extract(self, file_path):
        # type: (unicode) -> Dict
        """
        Extracts the text from an PSD File.
        """
        try:
            from psd_tools import PSDImage
        except ImportError:
            logger.error(
                'You need to install additional dependencies for this to work. '
                'Try installing the [documents] extras.'
            )
            raise
        try:
            psd = PSDImage.load(file_path)
            text = ''
            for group in psd.layers:
                text = text + ' ' + self._get_layer_text(group)
            return {
                'text': text,
                Analysis.UNIT_TYPE_WORDS: 1,
            }
        except Exception:
            logger.exception('An error occurred while extracting %s', file_path)
            raise LioxAnalyzerError(file_path)

    def _get_layer_text(self, obj):
        # type: (Union[Group, Layer]) -> str
        """
        Recursive function to go through layers of a PDF
        """
        # Installation of `psd_tools` has already been taken care of by try/catch
        # statement earlier
        from psd_tools.user_api.psd_image import Group, Layer  # pylint: disable=redefined-outer-name

        text = ''
        if isinstance(obj, Layer) and obj.text_data:
            text = obj.text_data.text
        elif isinstance(obj, Group):
            for item in obj.layers:
                text = text + ' ' + self._get_layer_text(item)
        return text
