# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from typing import Dict, Optional  # pylint: disable=unused-import

from .base import Extractor
from .mixins import MultiCodecExtractorMixin


logger = logging.getLogger(__name__)


class MultipleCodecExtractor(Extractor, MultiCodecExtractorMixin):
    id = 'multiple-codec-extractor'
    name = 'Multiple Codec Extractor'
    desc = 'Extracts text from a file using the io module and tries various codecs.'

    def extract(self, file_path, encoding=None):
        # type: (unicode, Optional[str]) -> Dict
        """
        Extracts text from a file using io.open. Attempts to use various codecs
        until we find one that matches.
        """
        text = self.multi_codec_read_file(file_path, encoding)
        return {
            'text': text,
        }
