# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging

from typing import Dict  # pylint: disable=unused-import

from .base import Extractor
from ..analysis import Analysis
from ..exceptions import LioxAnalyzerError
from ..text_utils import exclude_no_translate_attributed_tags, remove_markup_tags


logger = logging.getLogger(__name__)


class XmlExcludeNoTranslateExtractor(Extractor):
    id = 'xml-exclude-no-translate-extractor'
    name = 'XML Exclude No-Translate Extractor'
    desc = 'Extracts text from an XML type file, removing tags attributed translate="no".'

    def extract(self, file_path):
        # type: (unicode) -> Dict
        """
        Extracts the text from an XML File.
        """
        try:
            from bs4 import BeautifulSoup
        except ImportError:
            logger.error(
                'You need to install additional dependencies for this to work. '
                'Try installing the [xml] extras.'
            )
            raise
        try:
            with open(file_path, 'r') as file_obj:
                file_content = file_obj.read()
            soup = BeautifulSoup(file_content, 'xml')

            # remove tags attributed translate="no"
            soup = exclude_no_translate_attributed_tags(soup)

            text = remove_markup_tags(soup.get_text())

            return {
                'text': text,
                Analysis.UNIT_TYPE_PAGES: 1,
            }
        except Exception:
            logger.exception('An error occurred while extracting %s', file_path)
            raise LioxAnalyzerError(file_path)
