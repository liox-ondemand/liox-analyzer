# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging
import re

from typing import Dict, Optional  # pylint: disable=unused-import

from .base import Extractor
from .mixins import MultiCodecExtractorMixin
from ..exceptions import LioxAnalyzerError, InvalidFileError
from ..text_utils import get_string_values, remove_markup_tags


logger = logging.getLogger(__name__)


class YamlExtractor(Extractor, MultiCodecExtractorMixin):
    id = 'yaml-extractor'
    name = 'YAML File Extractor'
    desc = 'Extracts text from a YAML File.'

    def extract(self, file_path, encoding=None):
        # type: (unicode, Optional[str]) -> Dict
        """
        Extracts the text from a YAML file using
        """
        try:
            import yaml
            from yaml.composer import ComposerError
            from yaml.constructor import ConstructorError
            from yaml.parser import ParserError
            from yaml.scanner import ScannerError
        except ImportError:
            logger.error(
                'You need to install additional dependencies for this to work. '
                'Try installing the [software] extras.'
            )
            raise
        try:
            text = ''

            file_content = self.multi_codec_read_file(file_path, encoding)
            try:
                text += get_string_values(yaml.load(file_content))
            except (ComposerError, ScannerError, TypeError):
                with open(file_path, 'r') as file_:
                    for line in file_.readlines():
                        try:
                            text += ' ' + line.split(':', 1)[1] + '.'
                        except (AttributeError, IndexError):
                            pass

            # stripping out variables, anchors, and aliases
            text = re.sub(r'%\{\w+\}', ' ', text)
            text = re.sub(r'&\w+', ' ', text)
            text = re.sub(r'\*\w+', ' ', text)

            text = remove_markup_tags(text)

            return {
                'text': text,
            }
        except (ParserError, ConstructorError) as e:
            logger.exception('An error occurred while extracting %s', file_path)
            raise InvalidFileError(e.problem_mark.line+1, e.problem_mark.column+1, e.problem, e.problem_mark.get_snippet(), file_path)
        except Exception:
            logger.exception('An error occurred while extracting %s', file_path)
            raise LioxAnalyzerError(file_path)





