# pylint: skip-file

# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import bleach
import datetime
import six
import sys

from decimal import Decimal
from xml.sax.saxutils import unescape

from .exceptions import LioxUnicodeDecodeError


def exclude_no_translate_attributed_tags(soup):
    """
    Expects and returns a BeautifulSoup instance with tags
    attributed with translate='no' are removed.
    """
    for tag in soup.find_all(translate='no'):
        tag.extract()
    return soup


def remove_markup_tags(text):
    """
    Removes tags from text including escaped tags i.e. "&lt;li&gt;" (<li>)

    Note: This will replace escaped characters i.e.:
        &quot; = "
        &amp; = &
        &apos; = '

    >>> remove_markup_tags('&lt;li&gt;')
    u''
    >>> remove_markup_tags('<li>text</li>')
    u'text'
    >>> remove_markup_tags('<h1>text</h1>')
    u'text'
    >>> remove_markup_tags('<h1>text')
    u'text'
    >>> remove_markup_tags('&quot;')
    u'"'
    >>> remove_markup_tags('&amp;')
    u'&'
    >>> remove_markup_tags('&apos;')
    u"'"
    >>> remove_markup_tags(None)
    >>>
    """
    try:
        # We need to add unescape after bleach.clean because escaped characters will not be replaced
        # i.e. &amp; will remain &amp; without unescape after bleach.clean
        # >>> bleach.clean(unescape('&amp;'), tags=[], strip=True)
        # u'&amp;'
        if not isinstance(text, unicode):
            # Check if the text is not unicode, decode it first with the ``ignore`` option because
            # ``bleach.clean`` executes ``force_unicode`` on the text but for characters that could
            # not be decoded it raises UnicodeDecodeError.  Using ``ignore`` will remove the character.
            text = text.decode('utf-8', errors='ignore')
        return unescape(bleach.clean(unescape(text), tags=[], strip=True))
    except AttributeError:
        # This could be that text=None
        return text


def get_string_values(obj):
    """
    Recursive function to pull strings out of a list or nested dictionary.
    """
    if isinstance(obj, str):
        return obj
    elif isinstance(obj, list):
        return ' '.join([get_string_values(item) for item in obj])
    elif isinstance(obj, dict):
        return ' '.join([get_string_values(value) for value in obj.values()])
    else:
        return unicode(obj)


if six.PY3:
    memoryview = memoryview
    buffer_types = (bytes, bytearray, memoryview)
else:
    # memoryview and buffer are not strictly equivalent, but should be fine for
    # django core usage (mainly BinaryField). However, Jython doesn't support
    # buffer (see http://bugs.jython.org/issue1521), so we have to be careful.
    if sys.platform.startswith('java'):
        memoryview = memoryview
    else:
        memoryview = buffer
        buffer_types = (bytearray, memoryview)


def smart_text(s, encoding='utf-8', strings_only=False, errors='strict'):
    """
    Returns a text object representing 's' -- unicode on Python 2 and str on
    Python 3. Treats bytestrings using the 'encoding' codec.
    If strings_only is True, don't convert (some) non-string-like objects.
    """
    return force_text(s, encoding, strings_only, errors)


_PROTECTED_TYPES = six.integer_types + (
    type(None), float, Decimal, datetime.datetime, datetime.date, datetime.time
)


def is_protected_type(obj):
    """Determine if the object instance is of a protected type.
    Objects of protected types are preserved as-is when passed to
    force_text(strings_only=True).
    """
    return isinstance(obj, _PROTECTED_TYPES)


def force_text(s, encoding='utf-8', strings_only=False, errors='strict'):
    """
    Similar to smart_text, except that lazy instances are resolved to
    strings, rather than kept as lazy objects.
    If strings_only is True, don't convert (some) non-string-like objects.
    """
    # Handle the common case first for performance reasons.
    if issubclass(type(s), six.text_type):
        return s
    if strings_only and is_protected_type(s):
        return s
    try:
        if not issubclass(type(s), six.string_types):
            if six.PY3:
                if isinstance(s, bytes):
                    s = six.text_type(s, encoding, errors)
                else:
                    s = six.text_type(s)
            elif hasattr(s, '__unicode__'):
                s = six.text_type(s)
            else:
                s = six.text_type(bytes(s), encoding, errors)
        else:
            # Note: We use .decode() here, instead of six.text_type(s, encoding,
            # errors), so that if s is a SafeBytes, it ends up being a
            # SafeText at the end.
            s = s.decode(encoding, errors)
    except UnicodeDecodeError as e:
        if not isinstance(s, Exception):
            raise LioxUnicodeDecodeError(s, *e.args)
        else:
            # If we get to here, the caller has passed in an Exception
            # subclass populated with non-ASCII bytestring data without a
            # working unicode method. Try to handle this without raising a
            # further exception by individually forcing the exception args
            # to unicode.
            s = ' '.join(force_text(arg, encoding, strings_only, errors)
                         for arg in s)
    return s


def smart_bytes(s, encoding='utf-8', strings_only=False, errors='strict'):
    """
    Returns a bytestring version of 's', encoded as specified in 'encoding'.
    If strings_only is True, don't convert (some) non-string-like objects.
    """
    return force_bytes(s, encoding, strings_only, errors)


def force_bytes(s, encoding='utf-8', strings_only=False, errors='strict'):
    """
    Similar to smart_bytes, except that lazy instances are resolved to
    strings, rather than kept as lazy objects.
    If strings_only is True, don't convert (some) non-string-like objects.
    """
    # Handle the common case first for performance reasons.
    if isinstance(s, bytes):
        if encoding == 'utf-8':
            return s
        else:
            return s.decode('utf-8', errors).encode(encoding, errors)
    if strings_only and is_protected_type(s):
        return s
    if isinstance(s, memoryview):
        return bytes(s)
    if not isinstance(s, six.string_types):
        try:
            if six.PY3:
                return six.text_type(s).encode(encoding)
            else:
                return bytes(s)
        except UnicodeEncodeError:
            if isinstance(s, Exception):
                # An Exception subclass containing non-ASCII data that doesn't
                # know how to print itself properly. We shouldn't raise a
                # further exception.
                return b' '.join(force_bytes(arg, encoding, strings_only, errors)
                                 for arg in s)
            return six.text_type(s).encode(encoding, errors)
    else:
        return s.encode(encoding, errors)


def extract_from_markup(html_content):
    # type: (str) -> str
    """
    Uses BeautifulSoup to extract text from HTML.
    """
    try:
        from bs4 import BeautifulSoup
    except ImportError:
        logger.error(
            'You need to install additional dependencies for this to work.'
            'Try running `pip install liox_file_analyzer[xml]`'
        )
        raise
    soup = BeautifulSoup(html_content)

    # remove tags attributed translate="no"
    soup = exclude_no_translate_attributed_tags(soup)

    text = ''

    for alt in soup.find_all(alt=True):
        text += ". " + alt['alt']

    for title in soup.find_all(title=True):
        text += ". " + title['title']

    for title_tag in soup(['title']):
        text += ". " + title_tag.get_text()

    # remove invisible parts
    for script in soup(['script', 'style', 'head']):
        script.decompose()

    text += '. ' + soup.get_text('. ')

    text = remove_markup_tags(text)
    return text


if six.PY3:
    smart_str = smart_text
    force_str = force_text
else:
    smart_str = smart_bytes
    force_str = force_bytes
    # backwards compatibility for Python 2
    smart_unicode = smart_text
    force_unicode = force_text

smart_str.__doc__ = """
Apply smart_text in Python 3 and smart_bytes in Python 2.
This is suitable for writing to sys.stdout (for instance).
"""

force_str.__doc__ = """
Apply force_text in Python 3 and force_bytes in Python 2.
"""
