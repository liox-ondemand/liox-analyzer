# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

import logging
import os

from unittest import TestCase

from liox_analyzer import file_analyzer

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class AnalyzerTestCase(TestCase):

    test_files_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                  'test_files')

    def test_analyze_json_file(self):
        file_name = '630_sample.json'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        self.assertEqual(630, analysis.words)

    def test_analyze_non_ascii_json_file(self):
        file_name = '29_non_ascii.json'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        self.assertEqual(29, analysis.words)

    def test_analyze_invalid_json_file(self):
        file_name = 'invalid_sample.json'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertFalse(analysis.successful)
        self.assertTrue(analysis.errors)
        exception = analysis.errors[0]
        self.assertEquals(exception.filename, file_name)
        self.assertEquals(exception.lineno, 8)
        self.assertEquals(exception.colno, 43)
        self.assertEquals(exception.errant_line, '    "picture": "http://placehold.it/32x32,\n')
        self.assertEquals(exception.message, u'Error: Parse error on line 8, column 43\nInvalid control character %r at:     "picture": "http://placehold.it/32x32,\n\n--^')

    def test_analyze_text_file(self):
        file_name = '302_test.txt'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        self.assertEqual(302, analysis.words)

    def test_analyze_audio_file(self):
        file_name = '1_audio.mp3'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        self.assertEqual(1, analysis.minutes)

    def test_analyze_excel_file(self):
        file_name = '1564_test.xlsx'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        self.assertEqual(1564, analysis.words)

    def test_analyze_excel_file_with_hidden_sheet(self):
        file_name = '552_hidden_sheet.xlsx'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        self.assertEqual(552, analysis.words)

    def test_analyze_ascii_excel_file(self):
        file_name = '2_ascii.xlsx'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        self.assertEqual(analysis.words, 2)

    def test_analyze_doc_file(self):
        file_name = '39_testword.doc'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        expected_words = 39
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_docx_file(self):
        file_name = '39_testword.docx'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        expected_words = 39
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_new_docx_file(self):
        file_name = '20_testdocx.docx'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        expected_words = 20
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_docx_empty_headers_file(self):
        file_name = '863_doc_empty_headers.docx'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        expected_words = 863
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_docx_tables_file(self):
        file_name = '235_doc_tables.docx'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        expected_words = 235
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_docx_tables2_file(self):
        file_name = '126_doc_tables2.docx'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        expected_words = 126
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_ascii_docx_file(self):
        file_name = '2_ascii.docx'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        self.assertEqual(analysis.words, 2)

    def test_analyze_docx_w_diff_elements_file(self):
        file_name = '30_hdrftrtbltxbx.docx'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        self.assertEqual(analysis.words, 30)

    def test_analyze_pdf_file(self):
        file_name = 'test_pdf.pdf'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        self.assertEqual(10, analysis.words)
        self.assertEqual(2, analysis.pages)
        self.assertEqual(2, analysis.standardized_pages)

    def test_analyze_pptx_file_with_table(self):
        file_name = '3_992_slides_w_table.pptx'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        self.assertEqual(992, analysis.words)
        self.assertEqual(3, analysis.pages)
        self.assertEqual(3, analysis.standardized_pages)

    def test_analyze_pptx_file_no_notes(self):
        file_name = '4_20_simple.pptx'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        self.assertEqual(20, analysis.words)
        self.assertEqual(4, analysis.pages)
        self.assertEqual(4, analysis.standardized_pages)

    def test_analyze_pptx_file_notes(self):
        file_name = '4_29_simple_with_note.pptx'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        self.assertEqual(29, analysis.words)
        self.assertEqual(20, analysis.slides_words)
        self.assertEqual(9, analysis.notes_words)
        self.assertEqual(4, analysis.pages)
        self.assertEqual(4, analysis.standardized_pages)

    def test_analyze_video_file(self):
        file_name = '1_sample.mp4'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        self.assertEqual(1, analysis.minutes)

    def test_analyze_video_with_audio_only_file(self):
        file_name = '1_audo_only.mp4'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        self.assertEqual(1, analysis.minutes)

    def test_analyze_wmv_file(self):
        file_name = 'sample.wmv'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        self.assertEqual(1, analysis.minutes)

    def test_analyze_avi_file(self):
        file_name = 'sample.avi'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        self.assertEqual(1, analysis.minutes)

    def test_analyze_image_file(self):
        file_name = 'sample_graphic.jpg'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        self.assertEqual(1, analysis.pages)

    def test_analyze_resource_file(self):
        file_name = '100_sample.resx'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        self.assertEqual(100, analysis.words)

    def test_analyze_tmx_file(self):
        file_name = '8_test.tmx'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        expected_words = 8
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_po_file(self):
        file_name = '10_test.po'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        expected_words = 10
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_xlf_file_one(self):
        file_name = '23_test_w_cdata.xliff'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        expected_words = 23
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_xlif_file_two(self):
        file_name = '12_test_wo_cdata.xliff'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        expected_words = 12
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_xlif_file_three(self):
        file_name = '23_test_w_cdata_and_translate_no.xliff'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        expected_words = 23
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_xml_file(self):
        file_name = '65_test.xml'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        expected_words = 65
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_sgml_file(self):
        file_name = '6_sample.sgml'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        expected_words = 6
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_flglo(self):
        file_name = '641_glossary.flglo'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        expected_words = 641
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_yml_file(self):
        file_name = '10_sample.yml'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        expected_words = 10
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_parser_error_yml_file(self):
        file_name = 'parser_error_sample.yml'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertFalse(analysis.successful)
        exception = analysis.errors[0]
        self.assertEquals(exception.filename, file_name)
        self.assertEquals(exception.lineno, 2)
        self.assertEquals(exception.colno, 1)
        self.assertEquals(exception.msg, "expected '<document start>', but found '<block mapping start>'")
        self.assertEquals(exception.errant_line, '    invoice: 34843\n    ^')
        self.assertEquals(exception.message, u'Error: Parse error on line 2, column 1\nexpected \'<document start>\', but found \'<block mapping start>\':     invoice: 34843\n    ^\n--^')

    def test_analyze_constructor_error_yml_file(self):
        file_name = 'constructor_error_sample.yml'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertFalse(analysis.successful)
        exception = analysis.errors[0]
        self.assertEquals(exception.filename, file_name)
        self.assertEquals(exception.lineno, 1)
        self.assertEquals(exception.colno, 3)
        self.assertEquals(exception.msg, "found unacceptable key (unhashable type: 'list')")
        self.assertEquals(exception.errant_line, '    ? - 0\n      ^')
        self.assertEquals(exception.message, u"Error: Parse error on line 1, column 3\nfound unacceptable key (unhashable type: 'list'):     ? - 0\n      ^\n--^")

    def test_analyze_zip_file(self):
        file_name = '348_sample.zip'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        expected_words = 348
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_invalid_zip_file(self):
        file_name = 'invalid_zip.zip'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertFalse(analysis.successful)
        self.assertEquals(len(analysis.errors), 1)

    def test_analyze_properties_file(self):
        file_name = '25_sample.properties'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        expected_words = 25
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_strings_file(self):
        file_name = '89_sample.strings'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        expected_words = 89
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_srt_file(self):
        file_name = '12_sample.srt'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        expected_words = 12
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_vtt_file(self):
        file_name = '20_sample.vtt'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        expected_words = 20
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_ascii_vtt_file(self):
        file_name = '20_ascii.vtt'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        expected_words = 20
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_mif_file(self):
        file_name = '15_sample.mif'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        expected_words = 15
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_erb_file(self):
        file_name = '4_sample.erb'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        expected_words = 4
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_html_file_one(self):
        file_name = '3_no_translate.html'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        expected_words = 3
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_html_file_two(self):
        file_name = '3_no_translate_w_yes_translate.html'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        expected_words = 3
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_flsnp_file(self):
        file_name = '4_snippet.flsnp'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        expected_words = 4
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_idml_file(self):
        file_name = '14_sample.idml'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        expected_words = 14
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_rtf_file(self):
        file_name = '93_sample.rtf'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        expected_words = 93
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_psd_file(self):
        file_name = '9_sample.psd'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        expected_words = 9
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_epub_file(self):
        file_name = '22_sample.epub'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        expected_words = 22
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)
        self.assertTrue(analysis.pages, 3)

    def test_analyze_fltoc_file(self):
        file_name = '20_topic.fltoc'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        expected_words = 20
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_php(self):
        file_name = 'php_sample.php'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        expected_words = 6
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)


class StoryAnalyzerTestCase(TestCase):

    test_files_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                  'test_files/story_files')

    def test_analyze_story_file_one(self):
        file_name = '2531_7_0_storyist_getting_started.story'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        self.assertEqual(7, analysis.pages)
        expected_words = 2531
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)
        self.assertEqual(analysis.minutes, 0)

    def test_analyze_story_file_two(self):
        file_name = '0_0_6_video_formats.story'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        self.assertEqual(0, analysis.pages)
        self.assertEqual(0, analysis.words)
        self.assertEqual(analysis.minutes, 6)

    def test_analyze_story_file_three(self):
        file_name = '5_1_0_storyist_novel.story'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        self.assertEqual(1, analysis.pages)
        expected_words = 5
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)
        self.assertEqual(analysis.minutes, 0)

    def test_analyze_story_file_four(self):
        file_name = '90_1_9_storyline_loremipsum_w_audio.story'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        self.assertEqual(1, analysis.pages)
        expected_words = 90
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)
        self.assertEqual(analysis.minutes, 9)

    def test_analyze_story_file_five(self):
        file_name = '2_1_3_storyline_words_w_video.story'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        self.assertEqual(1, analysis.pages)
        expected_words = 2
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)
        self.assertEqual(analysis.minutes, 3)


class OdfAnalyzerTestCase(TestCase):

    test_files_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                  'test_files/odf_files')

    def test_analyze_odf_file_one(self):
        file_name = '1233_7_odf_file.odf'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        self.assertEqual(7, analysis.pages)
        expected_words = 1233
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_odf_file_two(self):
        file_name = '302_1_odf_file.ott'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        self.assertEqual(1, analysis.pages)
        expected_words = 302
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_odf_file_three(self):
        file_name = '302_2_odf_file.odt'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        self.assertEqual(2, analysis.pages)
        expected_words = 302
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)

    def test_analyze_odf_file_four(self):
        file_name = '311_1_odf_file.ods'
        file_path = os.path.join(self.test_files_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        self.assertEqual(1, analysis.pages)
        expected_words = 311
        self.assertTrue(abs(analysis.words - expected_words) // expected_words < 0.15)


class AnalyzerWithNonAsciiFilePathTestCase(TestCase):
    utf_8_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                  'test_files/utf_8/волн')

    def test_analyze_video_file_utf_8(self):
        file_name = '1_sample.mp4'
        file_path = os.path.join(self.utf_8_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        self.assertEqual(1, analysis.minutes)

    def test_analyze_wmv_file_utf_8(self):
        file_name = 'sample.wmv'
        file_path = os.path.join(self.utf_8_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        self.assertEqual(1, analysis.minutes)

    def test_analyze_avi_file_utf_8(self):
        file_name = 'sample.avi'
        file_path = os.path.join(self.utf_8_dir, file_name)
        analysis = file_analyzer.analyze(file_path=file_path)
        self.assertTrue(analysis.successful)
        self.assertEqual(1, analysis.minutes)
